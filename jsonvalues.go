package jsonvalues

import (
	"strconv"
	"strings"

	"github.com/json-iterator/go"
)

//PathParser get values from json according dynamic path
type PathParser struct {
	paths []interface{}
}

//GetValue function return slice of values extracted by path. If value only one it return slice with one element.
func (pp *PathParser) GetValue(data []byte) (res []string) {
	// return jsoniter.Get(data, pp.paths...).ToString()
	val := jsoniter.Get(data, pp.paths...)
	if val.Size() > 1 {
		for i := 0; i < val.Size(); i++ {
			res = append(res, val.Get(i).ToString())
		}
	} else {
		res = append(res, val.ToString())
	}
	return res
}

//NewParser method return new instance
func NewParser(path string) PathParser {
	pp := PathParser{}
	s := strings.Split(strings.Trim(path, "/"), "/")
	for _, p := range s {
		d, err := strconv.Atoi(p)
		if err != nil {
			if p == "*" {
				pp.paths = append(pp.paths, '*')
			} else {
				pp.paths = append(pp.paths, p)
			}
		} else {
			pp.paths = append(pp.paths, d)
		}
	}
	return pp
}
