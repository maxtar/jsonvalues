package jsonvalues

import (
	"sort"
	"testing"

	"github.com/json-iterator/go"
)

const testJSON = `
{
    "ID": 1,
    "Name": "Reds",
    "Colors": [
        "Crimson",
        "Red",
        "Ruby",
        "Maroon"
    ],
    "items": [
        {
            "item": {
                "id": 234
            }
        },
        {
            "item": {
                "id": 123
            }
        },
        {
            "item": {
                "id": 541
            }
        }
    ]
}
`

func TestGetValue(t *testing.T) {
	testData := []struct {
		path string
		exp  []string
	}{
		{"Colors", []string{"Crimson", "Red", "Ruby", "Maroon"}},
		{"items/*/item/id", []string{"234", "123", "541"}},
		{"items/1/item/id", []string{"123"}},
		{"item/1/item/id", []string{""}},
		{"/items/*/item/id/", []string{"234", "123", "541"}},
		{"/items/1/item/id", []string{"123"}},
	}

	json := []byte(testJSON)
	for _, test := range testData {
		pp := NewParser(test.path)
		res := pp.GetValue(json)
		//Check size
		if len(res) != len(test.exp) {
			t.Errorf("Expected %d, but was %d", len(test.exp), len(res))
		}
		//Check equals
		sort.Strings(test.exp)
		sort.Strings(res)
		for i := 0; i < len(test.exp); i++ {
			if test.exp[i] != res[i] {
				t.Errorf("Values are not equals - expected %q, but was %q", test.exp[i], res[i])
			}
		}
	}
}

func IgnoredTest(t *testing.T) {
	json := []byte(testJSON)
	exp := `["Crimson","Red","Ruby","Maroon"]`

	if res := jsoniter.Get(json, "items", '*', "item", "id"); res == nil {
		t.Errorf("Expected %s, but was %s\n", exp, res)
	} else {
		t.Logf(`Getted value:
Size: %d
Keys: %q
ToStrings: "%s"
LastError: %v
ValueType: %d
Interface: %v`, res.Size(), res.Keys(), res.ToString(), res.LastError(), res.ValueType(), res.GetInterface())
	}

}
